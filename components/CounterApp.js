import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import {connect} from 'react-redux'

function CounterApp(props) {
return (
    <View style={styles.container}>
    <TouchableOpacity style={styles.tombol}>
        <Text style={styles.textTombol} onPress={props.tambahSatu}>Tambah Satu</Text>
    </TouchableOpacity>
<Text style={styles.textAngka}>{props.angka}</Text>
    <TouchableOpacity style={styles.tombol}>
        <Text style={styles.textTombol} onPress={props.kurangSatu}>Kurang Satu</Text>
    </TouchableOpacity>
    <StatusBar style="auto" />
    </View>
);
}

function mapStateToProps(state){
    return {
        angka: state.angka
    }
}
function mapDispatchToProps(dispatch){
    return {
        tambahSatu: () => dispatch({type: 'TAMBAH_SATU'}),
        kurangSatu: () => dispatch({type: 'KURANG_SATU'}),
        
    }
}
export default connect(mapStateToProps, mapDispatchToProps) (CounterApp);

const styles = StyleSheet.create({
container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 50,
    alignItems: 'stretch',
    justifyContent: 'space-between',
},
tombol: {
    backgroundColor: '#2196F3',
    padding: 30,
    alignItems: 'center'
},
textTombol: {
    color: 'white',
    fontSize: 25,
    fontWeight: 'bold',
},
textAngka: {
    textAlign: 'center',
    fontSize: 200,
    fontWeight: 'bold',
}
});
