import React from 'react';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import CounterApp from './components/CounterApp'

const inisialisasiState = { angka: 0 }

function reducer (state=inisialisasiState, action) {
  switch (action.type) {
    case 'TAMBAH_SATU':
      return {angka: state.angka + 1 }
    case 'KURANG_SATU':
      return {angka: state.angka - 1 }
  }
  return state;
}

const store = createStore(reducer)
export default function App() {
  return (
    <Provider store={store}>
      <CounterApp />
    </Provider>
  );
}
